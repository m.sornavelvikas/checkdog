<?php

/*

* Template Name: Branchenbuch

*/

?>

<?php get_header(); ?>



	<div id="content">



		<div id="inner-content" class="container p borderlr">



			<!-- Adds top block with map on home page -->

						<?php 

						//if( is_home() OR is_front_page() ) {

						// Show top block is is home and no region set

						if( get_home_url() . '/' == "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" && !getCurrentRegionName() ) {

							wp_enqueue_script('maphighlight', get_template_directory_uri() . '/library/js/libs/maphighlight.js', array('jquery'), '', true);

							

							$recent = new WP_Query("pagename=HomeTopBlock");

							while($recent->have_posts()) {

								$recent->the_post();

							}

						    the_content();



						    ?>



						    <nav class="region-horizontal-list">

						    <?php //if( function_exists('getAdRegionList') )

						    	getAdRegionList(3); ?>

						    </nav>

						    

						    <?php

						} else {

							getAdRegionForm();

						}



						$loc_array = getCurrentRegionArray();

						$state = $loc_array[1];

						$county = $loc_array[2];

						

						?>



						<div class="headline cf">

							<div class="headline-search">

								<input id="search-category-input" type="text" value="<?php if(getCurrentCategoryName()) { echo getCurrentCategoryName(); } else { echo "Was suchen Sie?"; } ?>" data-empty="<?php if(getCurrentCategoryName()) { echo 'false'; } else { echo 'true'; } ?>" readonly>

								<a href="<?php echo getClearCategoryLink(); ?>" class="clear-field" id="clear-category-input"></a>

								<?php if( function_exists('getAdCategoryList') )

									getAdCategoryList(); ?>

							</div>

							<div class="headline-search">

								<input id="search-location-state-input" type="text" value="<?php if( $state ) { echo $state; } else { echo "In welchem Land?"; } ?>" data-empty="<?php if( $state ) { echo 'false'; } else { echo 'true'; } ?>" readonly>

								<a class="clear-field" id="clear-location-state-input"></a>

								<?php if( function_exists('getAdRegionStateList') )

									getAdRegionStateList(); ?>

							</div>

							<div class="headline-search">

								<input id="search-location-county-input" type="text" value="<?php if( $county ) { echo $county; } else { echo "In welchem Bundesland/Kanton?"; } ?>" data-empty="<?php if( $county ) { echo 'false'; } else { echo 'true'; } ?>" readonly>

								<a class="clear-field" id="clear-location-county-input"></a>

								<?php if( function_exists('getAdRegionCountyList') )

									getAdRegionCountyList(); ?>

							</div>

	<a class="button white" href="javascript:location.reload();"><?php _e( 'Suchen', 'stroschtheme' ); ?></a>

						</div>



			<div id="sidebar1" class="sidebar">

				<?php dynamic_sidebar('sidebar left'); ?>

				<?php // Banners left side

				if (function_exists('dynamic_sidebar')) : ?>

					<div class="banners-left-container">

						<?php dynamic_sidebar('Banners Left'); ?>

					</div>

				<?php endif; ?>

                <?php

				require_once 'Mobile_Detect.php';

				$detect = new Mobile_Detect;

				$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

				if($deviceType=='computer'){ ?>

				

				<?php if ( is_active_sidebar( 'sidebar_banner_left_sidebar' ) ) : ?>

				<?php dynamic_sidebar( 'sidebar_banner_left_sidebar' ); ?>

				<?php endif; ?>

				

				<?php if ( is_active_sidebar( 'partnerstorecode' ) ) : ?>

				<?php dynamic_sidebar( 'partnerstorecode' ); ?>

				<?php endif; ?>

				

				<?php } ?>

			</div>



			<main id="main" class="cf" role="main">



				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                



				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">



					<?php

						the_content();



						wp_link_pages( array(

							'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',

							'after'       => '</div>',

							'link_before' => '<span>',

							'link_after'  => '</span>',

						) );

					?>

				</article>



				<?php endwhile; endif; ?>



				<?php // Partner Store Code

				//if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Partner Store Code')) : ?>

				<?php //endif; ?>



			</main>



		</div>



	</div>


<?php get_footer(); ?>