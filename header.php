<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->

<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->

<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->

<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
<meta name="verification" content="5fe9c119d82263b79fb284fef81dffca" />
<meta http-equiv="content-type" content="text/html; charset=utf-8">
</meta>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>
<?php wp_title(''); ?>
</title>
<meta name = "seobility" content = "219897c56b738c3543ff68b6dd5d0127">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png" alt="Apple">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/library/images/favicon.ico">

<!--[if IE]>

			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>library/images/favicon.ico">


		<![endif]-->

<meta name="msapplication-TileColor" content="#f01d4f">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<?php wp_head(); ?>
</head>
<?php
$blog_id = get_current_blog_id();
?>
<body <?php if( get_home_url() . '/' == "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" && !getCurrentRegionName() ) { body_class('home-url'); } else { body_class('subpage'); } ?> >
<!---<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42748738-19', 'auto');
  ga('send', 'pageview');

</script>--->
<div id="overall">
<div id="container">
<header class="header <?php echo $headerclass; ?>" role="banner">
  <div class="container borderlr">
    <div class="top cf ph">
      <div class="left hide_on_tablet">
        <ul>
          <li>Über <a href="https://hochzeit-selber-planen.com/shop/accessoires-hochzeit/" target="_blank">10.000 Produkte</a> verfügbar</li>
          <li>Kostenloser Versand</li>
          <li>Lieferung in 1-3 Werktagen</li>
        </ul>
      </div>
      <div class="right">
        <ul>
          <li>
            <?php if($blog_id==1){?>
            <a href="<?php echo get_page_link(175); ?>">
            <?php _e( 'Inserat/Banner buchen', 'stroschtheme' ); ?>
            </a>
            <?php }else{ ?>
            <a href="<?php echo get_page_link(42); ?>">
            <?php _e( 'Anzeige Aufgeben', 'stroschtheme' ); ?>
            </a>
            <?php } ?>
          </li>
          <?php if ( is_user_logged_in() ) { ?>
          <li>
            <?php if($blog_id==1){?>
            <a href="<?php echo get_page_link(167); ?>">
            <?php _e( 'Mein Profil', 'stroschtheme' ); ?>
            </a>
            <?php }else{ ?>
            <a href="<?php echo get_page_link(7); ?>">
            <?php _e( 'Ihr Profil', 'stroschtheme' ); ?>
            </a>
            <?php } ?>
          </li>
          <li>
            <?php if($blog_id==1){?>
            <a href="<?php echo get_site_url(); ?>/wp-admin/admin.php?page=awpcp-panel" target="_blank">
            <?php _e( 'Meine Inserate/Banner', 'stroschtheme' ); ?>
            </a>
            <?php }else{ ?>
            <a href="<?php echo get_site_url(); ?>/wp-admin/admin.php?page=awpcp-panel" target="_blank">
            <?php _e( 'Meine Anzeigen', 'stroschtheme' ); ?>
            </a>
            <?php } ?>
          </li>
          <li><a href="<?php echo wp_logout_url( home_url() ); ?>"><b>
            <?php _e( 'Logout', 'stroschtheme' ); ?>
            </b></a></li>
          <?php } else { ?>
          <li>
            <?php if($blog_id==1){?>
            <a href="<?php echo get_page_link(164); ?>">
            <?php _e( 'Login', 'stroschtheme' ); ?>
            </b></a>
            <?php }else{ ?>
            <a href="<?php echo get_page_link(4); ?>">
            <?php _e( 'Login', 'stroschtheme' ); ?>
            </b></a>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="middle cf p"> <a href="javascript:void(0)" class="mobile_serach_open"><i class="ion-search"></i></a><a href="<?php echo get_home_url(); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/logo_50@2x.png" alt="Logo" /></a>
      <?php
        require_once 'Mobile_Detect.php';
        $detect = new Mobile_Detect;
        $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
        if($deviceType !='computer'){ ?>
      <div class="mobile-tab-search-container">
        <?php } ?>
        <div class="row search global-search">
          <div class=" small-12 columns">
            <div class="row collapse">

              <!-- Search field -->

              <div class="small-12 large-12  columns">
                <div class="keyword-global">
                  <select name="shop-pages-dropdown-global" id="shop-pages-dropdown-global">
                    <option value="all">Alle</option>
                    <?php
                        global $switched;
                        switch_to_blog(1);
                        $menu_id = '9';
                        $menuitems = wp_get_nav_menu_items( $menu_id, array( 'order' => 'DESC' ) );
                        foreach( $menuitems as $item ){
                        $id = get_post_meta( $item->ID, '_menu_item_object_id', true );
                        $page = get_page( $id );
                        $link = get_page_link( $id );
                        ?>
                    <option value="<?php echo  $id; ?>" data-link="<?php echo $link; ?>"><?php echo $page->post_title; ?></option>
                    <?php } ?>
                    <?php  restore_current_blog(); ?>
                  </select>
                </div>
                <input class="search-field-global" id="search-field-global" placeholder="Gesamten Shop durchsuchen..." type="text">
                <button type="submit" id="global-search-submit">Suche</button>
                <img src="<?php echo get_template_directory_uri(); ?>/images/loader1.gif" alt="loader" class="global-search-loder">
                <div class="global-search-results">
                  <ul id="global-search-results-list">

                    <!--search results-->

                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if($deviceType !='computer'){ ?>
      </div>
      <?php } ?>
      <div class="right hide_on_phone">


        <div id="mini_cart_header"> <a id="warenkorb-teaser" class="warenkorb-teaser" href="/shop/accessoires-hochzeit/"> <span class="icon" title="Warenkorb"></span>
          <div class="text"> <strong>Keine Artikel</strong><br>
            im Warenkorb </div>
          </a> </div>
        <ul class="mobile-top-link">
          <li>Über 10.000 Produkte verfügbar</li>
          <li>Kostenloser Versand</li>
          <li>Lieferung in 1-3 Werktagen</li>
        </ul>
        <!-- <ul class="social">
								<li><a href="#" class="fb"><i class="ion-social-facebook"></i></a></li>
								<li><a href="#" class="yt"><i class="ion-social-youtube"></i></a></li>
							</ul> -->

      </div>
    </div>
    <div class="bottom cf ph">
      <?php // if (!is_page_template( 'amazon-curl.php' ) ) {  ?>
      <nav role="navigation">
      <?php switch_to_blog(1); ?>
        <?php wp_nav_menu(array(
				'container' => 'div',
		    	'container_class' => 'nav-desktop',
                'menu' => __( 'The Main Menu', 'bonestheme' ),
                'theme_location' => 'main-nav'
           )); ?>

        
        <?php  restore_current_blog(); ?>
        <button id="nav-mobile-toggle"><span class="stripe">Toggle Menu</span></button>
        <button id="sidebar-mobile-toggle"></button>
      </nav>
    </div>
    <?php // } ?>
  </div>
</header>
