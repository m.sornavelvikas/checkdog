<style>



body.category #main { float: left; padding: 0 30px; width: calc(100% - 400px);}



.col_2 { float:left; width:50%; padding:0 10px;}



.blog_main { margin:0 0 40px;}



.blg_thmb img { width:100%; height:auto;}



.blog_main h3 { height: 22px; overflow: hidden;}



.blog_info { font-size: 1.4rem; margin:0 0 15px;}



ul.blog_info_top { margin:0 0 10px; height: 40px; overflow: hidden;}



ul.blog_info_top li { list-style:none; display:inline-block; position:relative; margin-right:10px; font-size:11px; font-style:italic; color:#C1B8AF;}



ul.blog_info_top li:after { position:absolute; content:""; background:#C1B8AF; width:5px; height:1px; left:-9px; top:50%;}



ul.blog_info_top li:first-child:after { display:none;}



.blg_link { text-align:right;}



.blg_link a.btn { background: #ec3f7f; display: inline-block; color: #fff; padding: 5px 10px; font-size: 12px; font-style: italic;}







.clear { clear: both;}



.artcl_btn { margin:0 0 35px;}



.artcl_btn a.btn { display:block; text-align:center; border:1px solid #333; text-transform:uppercase; padding:10px}







.pagtn { text-align: right;}



.pagtn .wp-pagenavi a, .wp-pagenavi span { display:inline-block;}











@media (max-width: 479px) {



.col_2 { width:100%;}	







}



</style>



<?php get_header(); ?>



 <?php $options = get_option('payment_gateway_settings'); ?>



	<div id="content">



        



		<div id="inner-content" class="container p borderlr">







			<div id="sidebar1" class="sidebar">

               <?php dynamic_sidebar( 'sidebar-blog' ); ?>               

				<?php //dynamic_sidebar('sidebar left'); ?>



				<?php // Banners left side



				if ( is_active_sidebar('Banners Left') ) : ?>



					<div class="banners-left-container">



						<?php dynamic_sidebar('Banners Left'); ?>



					</div>



				<?php endif; ?>



                



                <?php



				require_once 'Mobile_Detect.php';



				$detect = new Mobile_Detect;



				$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');



				if($deviceType=='computer'){ ?>



				



				<?php if ( is_active_sidebar( 'sidebar_banner_left_sidebar' ) ) : ?>



				<?php dynamic_sidebar( 'sidebar_banner_left_sidebar' ); ?>



				<?php endif; ?>



				



				<?php if ( is_active_sidebar( 'partnerstorecode' ) ) : ?>



				<?php dynamic_sidebar( 'partnerstorecode' ); ?>



				<?php endif; ?>



				



				<?php } ?>



                



			</div>







			<main id="main" role="main">



              <div class="main_blog">



	          <?php



				if( have_posts()) : while ( have_posts() ) : the_post();



				



				$content = get_the_content($post->ID);



				$content =  strip_tags($content);



                $content_len = strlen($content);



				



				



				$post_admin_id = $post->post_author;



				$user_info = get_userdata($post_admin_id);



				$user_login = $user_info->user_login;



				$date = get_the_date('F j, Y', $post->ID );



				



				$category = get_the_category($post->ID);



			    $category_name = $category[0]->cat_name;



				?>



				



				



                	<div class="col_2">



                    	 <div class="blog_main">



                         	<div class="blg_thmb"> <a href="<?php the_permalink(); ?>">



			   <?php 



				if ( has_post_thumbnail() ){



				//$default_attr = array('class'=>"blog_img_".$post->ID,'alt'=>get_the_title($post->ID),'title'=>get_the_title($post->ID),);



				echo get_the_post_thumbnail($post->ID,'blog-thumb'); }



				?>



					</a>



					<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title($post->ID); ?></a></h3>



				  </div>



				  <div class="blog_info">



                  	<ul class="blog_info_top">



                    	<li>Von <?php echo $user_login; ?></li>



                        <li>in <?php echo $category_name; ?></li>



                        <li><?php echo $date; ?></li>



                    </ul>



					<p><?php echo substr($content,0,240);?><?php if( $content_len>240){echo '...';}?></p>



				  </div>



				         <div class="blg_link"><a class="btn" href="<?php the_permalink(); ?>">Weiter</a></div>



                         </div>



                    </div>				 



                  



				



				<?php endwhile; else : ?>



				<p>Keine Artikel gefunden.</p>



				<?php endif; ?>



                </div>



                <div class="clear"></div>



                <div class="artcl_btn">



                	<a href="<?php echo get_page_link($options[post_article_page]); ?>" class="btn">Artikel ver&ouml;ffentlichen</a>



                </div>



				<div class="pagtn">



				  <?php  wp_pagenavi(); wp_reset_postdata();?>



				</div>







			</main>







			<div id="sidebar2" class="sidebar">



				<?php dynamic_sidebar('sidebar right'); ?>



				<?php // Banners right side



				if ( is_active_sidebar('Banners Right') ) : ?>



					<div class="banners-right-container">



						<?php dynamic_sidebar('Banners Right'); ?>



					</div>



				<?php endif; ?>



                



                <?php if($deviceType=='computer'){ ?>







				<?php if ( is_active_sidebar( 'sidebar_banner_right' ) ) : ?>



                <?php dynamic_sidebar( 'sidebar_banner_right' ); ?>



                <?php endif; ?>



                



                <?php if ( is_active_sidebar( 'partnerstorecoderight' ) ) : ?>



				<?php dynamic_sidebar( 'partnerstorecoderight' ); ?>



				<?php endif; ?>



                



                <?php } ?>



                



			</div>







		</div>







	</div>







<?php get_footer(); ?>



